import { useState } from "react";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  {
    id: 'e1',
    title: "Car Insurance",
    amount: 204.55,
    date: new Date(2021, 3, 24),
  },
  {
    id: 'e2',
    title: "Plane Ticket",
    amount: 100.55,
    date: new Date(2021, 3, 25),
  },
  {
    id: 'e3',
    title: "Business Trip",
    amount: 105.55,
    date: new Date(2021, 3, 26),
  },
];

function App() {

  const [expenses, setExpenses] = useState(DUMMY_EXPENSES)
  

  const addExpenseHandler = expense => {
    setExpenses(prevExpenses => {
      return [expense, ...prevExpenses]
    })
  }

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />
    </div>
  );
}

export default App;
